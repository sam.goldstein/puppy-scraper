console.log(1);
const aws = require('aws-sdk');
const sns = new aws.SNS();
const s3 = new aws.S3();
const fetch = require('node-fetch');
console.log(1);
const jsdom = require('jsdom');
console.log(1);
const { JSDOM } = jsdom;
const STATE_KEY = 'state.json';
exports.handler = async (event, context) => {
  // Log the event argument for debugging and for use in local development.
  console.log(JSON.stringify(event, undefined, 2));
  const response = await fetch('https://otatpdx.org/adopt/')
  const data = await response.text();
  const dogs = {};

  // A chunk of data has been recieved.

  // The whole response has been received. Print out the result.
  console.log(data);
  const dom = new JSDOM(data);
  const h2s = dom.window.document.querySelectorAll('.title-rollover h2');
  console.log(h2s.length);
  h2s.forEach((h2) => {
    const inner = h2.innerHTML;
    const pair = inner.split(/<br>\s*/).map(i => { return i.trim(); });
    console.log(pair);
    dogs[pair[0]] = pair[1];
  });
  const state = await getState();
  console.log(state);
  console.log(dogs);
  if (JSON.stringify(dogs) !== JSON.stringify(state)) {
    console.log('dogs have changed');
    let message = 'Dogs Have Changed:\n';
    for (const [key, value] of Object.entries(dogs)) {
      if (value !== state[key]) {
        message += `${key} ${value}\n`;
      }
    }
    await notify(process.env.NOTIFY_SMS_NUMBER, message);

    await saveState(dogs);
  } else {
    console.log('no dogs have changed');
  }
  return dogs;
};

async function notify (number, message) {
  await sns.publish({
    Message: message,
    PhoneNumber: number
  }).promise();
}

async function getState () {
  var params = {
    Bucket: process.env.BUCKET_NAME,
    Key: STATE_KEY
  };
  try {
    console.log('getting state');
    const response = await s3.getObject(params).promise();
    console.log(response);
    return JSON.parse(response.Body.toString());
  } catch (e) {
    console.log(`caught error ${e}`);
    return {};
  }
}

async function saveState (data) {
  var params = {
    Bucket: process.env.BUCKET_NAME,
    Key: STATE_KEY,
    Body: JSON.stringify(data)
  };
  console.log('saving state');
  const response = await s3.putObject(params).promise();
  console.log(response);
}
