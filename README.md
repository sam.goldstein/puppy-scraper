# Puppy Scraper

Our family recently decided we were "ready" to adopt a dog.  However due to
COVID-19 there is a lot more people interested in adopting dogs so we are still
working on finding the right dog for our family.

I wrote this app to text message us when dog listings are updated on local dog
shelters.  This helps us cut down on compulsive checking of these websites.

I used Stackey, AWS Lambda, SNS, and S3 to build this app.
